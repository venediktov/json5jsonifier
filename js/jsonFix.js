jsonInput = document.getElementById('inputJson')
jsonOutput = document.getElementById('outputJson')

// Drop area

if (window.File && window.FileReader && window.FileList && window.Blob) {
    // Great success!
    function handleJSONDrop(evt) {
      evt.stopPropagation();
      evt.preventDefault();
      var files = evt.dataTransfer.files;
        // Loop through the FileList and read
        for (var i = 0, f; f = files[i]; i++) {
    
          // Only process json files.
            if (!f.type.match('application/json')) {
            continue;
          }
    
          var reader = new FileReader();
    
          // Closure to capture the file information.
          reader.onload = (function(theFile) {
            return function(e) {
                //Fix the file
                json5parsed = JSON5.parse(e.target.result)
                jsonFinal = JSON.stringify(json5parsed, null, 2); // stringify with tabs inserted at each level
                //Download
                download(jsonFinal, theFile.name, 'text/plain')
            };
          })(f);
    
          reader.readAsText(f);
        }
    }
  
    function handleDragOver(evt) {
      evt.stopPropagation();
      evt.preventDefault();
      evt.dataTransfer.dropEffect = 'copy'; // Explicitly show this is a copy.
    }
  
    // Setup the dnd listeners.
    var dropZone = document.getElementsByTagName('body')[0];
    dropZone.addEventListener('dragover', handleDragOver, false);
    dropZone.addEventListener('drop', handleJSONDrop, false);
    
  
  } else {
    alert('The File APIs are not fully supported in this browser.');
  }
  
// fix Json


// Download file
function download(data, filename, type) {
    var file = new Blob([data], {type: type});
    if (window.navigator.msSaveOrOpenBlob) // IE10+
        window.navigator.msSaveOrOpenBlob(file, filename);
    else { // Others
        var a = document.createElement("a"),
                url = URL.createObjectURL(file);
        a.href = url;
        a.download = filename;
        document.body.appendChild(a);
        a.click();
        setTimeout(function() {
            document.body.removeChild(a);
            window.URL.revokeObjectURL(url);  
        }, 0); 
    }
}

// document.getElementById('outputJson').addEventListener('click', function() {
//     jsonInputVal = jsonInput.value

//     json5parsed = JSON5.parse(jsonInputVal)
//     console.log(json5parsed)
    
//     jsonFinal = JSON.stringify(json5parsed, null, 2); // stringify with tabs inserted at each level
//     jsonOutput.value = jsonFinal

//     download(jsonFinal, 'file.json', 'text/plain')
// })